package ru.bokhan.tm.repository;

import ru.bokhan.tm.api.ICommandRepository;
import ru.bokhan.tm.constant.ArgumentConst;
import ru.bokhan.tm.constant.TerminalConst;
import ru.bokhan.tm.model.TerminalCommand;

import java.util.Arrays;

public class CommandRepository implements ICommandRepository {

    public static final TerminalCommand HELP = new TerminalCommand(
            TerminalConst.HELP, ArgumentConst.HELP, "Show help."
    );

    public static final TerminalCommand ABOUT = new TerminalCommand(
            TerminalConst.ABOUT, ArgumentConst.ABOUT, "Show developer info."
    );

    public static final TerminalCommand VERSION = new TerminalCommand(
            TerminalConst.VERSION, ArgumentConst.VERSION, "Show version info."
    );

    public static final TerminalCommand INFO = new TerminalCommand(
            TerminalConst.INFO, ArgumentConst.INFO, "Show information about system."
    );

    public static final TerminalCommand EXIT = new TerminalCommand(
            TerminalConst.EXIT, null, "Close application."
    );

    public static final TerminalCommand ARGUMENTS = new TerminalCommand(
            TerminalConst.ARGUMENTS, ArgumentConst.ARGUMENTS, "Show program arguments."
    );

    public static final TerminalCommand COMMANDS = new TerminalCommand(
            TerminalConst.COMMANDS, ArgumentConst.COMMANDS, "Show program commands."
    );

    public static final TerminalCommand[] TERMINAL_COMMANDS = new TerminalCommand[]{
            HELP, ABOUT, VERSION, INFO, COMMANDS, ARGUMENTS, EXIT
    };

    public final String[] commands = getCommands(TERMINAL_COMMANDS);

    public final String[] arguments = getArguments(TERMINAL_COMMANDS);

    private String[] getCommands(TerminalCommand... values) {
        if (values == null || values.length == 0) return new String[]{};
        final String[] result = new String[values.length];
        int index = 0;
        for (TerminalCommand value : values) {
            final String name = value.getName();
            if (name == null || name.isEmpty()) continue;
            result[index] = name;
            index++;
        }
        return Arrays.copyOfRange(result, 0, index);
    }

    private String[] getArguments(TerminalCommand... values) {
        if (values == null || values.length == 0) return new String[]{};
        final String[] result = new String[values.length];
        int index = 0;
        for (TerminalCommand value : values) {
            final String argument = value.getArgument();
            if (argument == null || argument.isEmpty()) continue;
            result[index] = argument;
            index++;
        }
        return Arrays.copyOfRange(result, 0, index);
    }

    @Override
    public TerminalCommand[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }

    @Override
    public String[] getCommands() {
        return commands;
    }

    @Override
    public String[] getArguments() {
        return arguments;
    }

}
