package ru.bokhan.tm.api;

import ru.bokhan.tm.model.TerminalCommand;

public interface ICommandRepository {

    TerminalCommand[] getTerminalCommands();

    String[] getCommands();

    String[] getArguments();

}
