package ru.bokhan.tm.api;

public interface ICommandController {

    void showInfo();

    void showAbout();

    void showVersion();

    void showArguments();

    void showCommands();

    void showHelp();

    void exit();

}
