package ru.bokhan.tm.service;

import ru.bokhan.tm.api.ICommandRepository;
import ru.bokhan.tm.api.ICommandService;
import ru.bokhan.tm.model.TerminalCommand;

public class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public TerminalCommand[] getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

    @Override
    public String[] getCommands() {
        return commandRepository.getCommands();
    }

    @Override
    public String[] getArguments() {
        return commandRepository.getArguments();
    }
}
